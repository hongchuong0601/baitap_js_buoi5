// Bài 1: Xét kết quả tuyển sinh

function ketqua(){
    var diemChuan=document.getElementById("diem-chuan").value*1
    var diemKhuvuc=document.getElementById("khu-vuc").value*1
    var diemDoituong=document.getElementById("doi-tuong").value*1
    var diemMon1=document.getElementById("mon1").value*1
    var diemMon2=document.getElementById("mon2").value*1
    var diemMon3=document.getElementById("mon3").value*1
    var diemTong3Mon= diemMon1 + diemMon2 + diemMon3
    var diemTong = diemTong3Mon + diemDoituong + diemKhuvuc
console.log({diemTong})
    if(diemMon1==0 || diemMon2==0 || diemMon3==0){
        document.getElementById("ket-qua").innerHTML=` Bạn đã rớt vì bị điểm liệt`
    } else if(diemChuan > diemTong){
        document.getElementById("ket-qua").innerHTML=` Bạn không đủ điểm để trúng tuyển ${diemTong}`
    } else if(diemTong >= diemChuan){
        document.getElementById("ket-qua").innerHTML=` Ban đả trúng tuyển với số điểm: ${diemTong} điểm`
    }
}

// Bài 2: Tính tiền điện

function ketqua1(){
    var hoTen=document.getElementById("ho-ten").value
    var soKw= document.getElementById("so-kw").value*1

    if(soKw<=50){
        var soTien=soKw*500
    } else if(soKw>50 && soKw<=100){
        var soTien= (50*500)+(soKw-50)*650
    } else if(soKw>100 && soKw<=200){
        var soTien= (50*500)+(50*650)+(soKw-100)*850
    } else if(soKw>200 && soKw<=350){
        var soTien= (50*500)+(50*650)+(100*850)+(soKw-200)*1100
    } else {
        var soTien= (50*500)+(50*650)+(100*850)+(150*1100)+(soKw-350)*1300
    }
    document.getElementById("ket-qua1").innerHTML=` Họ tên: ${hoTen} ; Tiền điện: ${soTien} VNĐ`
}

// Bài 3: Tính thuế TNCN

function ketqua2(){
    var hoTenTNCN=document.getElementById("ho-tenTNCN").value
    var tongThuNhap=document.getElementById("thu-nhap").value*1
    var soNguoiFL=document.getElementById("nguoi-phuthuoc").value*1

    if(tongThuNhap<= 60000000){
        var thueTNCN=(tongThuNhap-4000000-(soNguoiFL*1600000))*0.05
    } else if(tongThuNhap>60000000 && tongThuNhap<=120000000){
        var thueTNCN=(tongThuNhap-4000000-(soNguoiFL*1600000))*0.1
    } else if(tongThuNhap>120000000 && tongThuNhap<=210000000){
        var thueTNCN=(tongThuNhap-4000000-(soNguoiFL*1600000))*0.15
    } else if(tongThuNhap>210000000 && tongThuNhap<=384000000){
        var thueTNCN=(tongThuNhap-4000000-(soNguoiFL*1600000))*0.2
    } else if(tongThuNhap>384000000 && tongThuNhap<=624000000){
        var thueTNCN=(tongThuNhap-4000000-(soNguoiFL*1600000))*0.25
    } else if(tongThuNhap>624000000 && tongThuNhap<=960000000){
        var thueTNCN=(tongThuNhap-4000000-(soNguoiFL*1600000))*0.3
    } else {
        var thueTNCN=(tongThuNhap-4000000-(soNguoiFL*1600000))*0.35
    }
    
    document.getElementById("ket-qua2").innerHTML=` Tên: ${hoTenTNCN}; Tiền thuế TNCN: ${thueTNCN} VND`
}

// Bài 4: Tính tiền cáp



function ketqua3(){
    var maKH=document.getElementById("ma-kh").value
    var loaiKH=document.getElementById("loai-kh").value
    var soKenhCC=document.getElementById("kenh-caocap").value*1
    var soKenh=document.getElementById("kenh-ketnoi").value*1

    console.log({loaiKH})
    
    if(loaiKH=="ca-nhan"){
        var tienCap= 25 + soKenhCC*7.5
    } else if(loaiKH=="doanh-nghiep" && soKenh<=10) {
        var tienCap= 90 + soKenhCC*50
    } else if(loaiKH=="doanh-nghiep" && soKenh>10){
        var tienCap= 90 + soKenhCC*50 + (soKenh-10)*5
    }

    document.getElementById("ketqua3").innerHTML=` Mã khác hàng: ${maKH}; Tiền cáp: ${tienCap} $`
}